<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'username',
        'email',
        'password',
        'background_color',
        'text_color',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function links()
    {
        return $this->hasMany(Link::class);
    }

    public function visits(){
        $this->hasManyThrough(Visit::class,Link::class ); //pokazimigi samo visits samo za toj user za  toj link
    }

    public function getRouteKeyName()//OVA GO KLAVAME KAKO ZA SLUG DA MOZI DA LINKTTREE.TEST/MARKOSKI97 DA PREBARUVA MESTO LINKTREEE.TEST/1
    {
        return 'username';
    }
}
