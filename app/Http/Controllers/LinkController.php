<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LinkController extends Controller
{

    public function index()
    {
        $links=Auth::user()->links()
            ->withCount('visits')
            ->with('latest_visit')
            ->get();
       return view('links.index',compact('links'));
    }

    public function create()
    {
       return view('links.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'link' => 'required|url'
        ]);

        $link = Auth::user()->links()
            ->create($request->only(['name', 'link']));

        if ($link) {
            return redirect()->to('/dashboard/links');
        }

        return redirect()->back();
    }


    public function edit(Link $link)
    {
        if ($link->user_id !== Auth::id()) {
            return abort(404);
        }//Ako Linko ne pripaga na usero daj mu 404 (Ako si smeni nekoj sam id vo url)
        return view('links.edit',compact('link'));
    }


    public function update(Request $request,Link $link)
    {
        if ($link->user_id !== Auth::id()) {
            return abort(403);
        }//Ako Linko ne pripaga na usero daj mu 404 (Ako si smeni nekoj sam id vo url)

        $request->validate([
            'name' => 'required|max:255',
            'link' => 'required|url'
        ]);

       $link->update($request->only(['name','link']));

       return redirect()->to('/dashboard/links');
    }

    public function destroy(Link $link)
    {
        if ($link->user_id !== Auth::id()) {
            return abort(403);
        }//Ako Linko ne pripaga na usero daj mu 404 (Ako si smeni nekoj sam id vo url)

        $link->delete();
        return redirect()->to('/dashboard/links');
    }
}
