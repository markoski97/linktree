<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function edit()
    {
        $user=Auth::user();
        return view ('users.edit',compact('user'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'background_color' =>'required|size:7|starts_with:#',
            'text_color' =>'required|size:7|starts_with:#',
        ]);

        Auth::user()->update($request->only(['background_color','text_color']));

        return redirect()->back();
    }

    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }
}
